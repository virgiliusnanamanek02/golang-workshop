
# Learning Go Programming Journey 🚀

Welcome to my Go programming language learning repository! As a beginner diving into the world of Go, I intend to record every step and newfound knowledge in this readme. Let's get started!

## Explored Topics (*soon*)

## My Favorite Resources

- [Official Go Documentation](https://golang.org/doc/)
- [A Tour of Go](https://tour.golang.org/welcome/1)
- [Awesome Go](https://github.com/avelino/awesome-go)
- [Dasar Pemrograman Go](https://dasarpemrogramangolang.novalagung.com)

## License

MIT License - [Details](LICENSE)

Happy learning! 🚀


